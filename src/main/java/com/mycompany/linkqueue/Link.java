/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.linkqueue;

/**
 *
 * @author informatics
 */
public class Link {

    public long dData; // data item
    public Link next;

    public Link(long d) // constructor
    {
        dData = d;
    }

    public void displayLink() // display this link
    {
        System.out.print(dData + " ");
    }
    
}